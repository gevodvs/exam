FROM openjdk:17

COPY target/Hevod-0.0.1-SNAPSHOT.jar /app/Hevod-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "./app/Hevod-0.0.1-SNAPSHOT.jar"]

