
docker compose up -d

Scheduled service (cron) deletes rows that live more than 30 seconds every 15 sec
``` bash
curl --location --request POST 'http://localhost:8080/add' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "test",
    "uid": "test",
    "resultSize": 3
}'
```

``` bash
curl --location --request GET 'http://localhost:8080/get/{id}'
```

``` bash
curl --location --request POST 'http://localhost:8080/update' \
--header 'Content-Type: application/json' \
--data-raw '{
    "uid": "test",
    "data": 2.01,
    "updateTime": "2023-09-23 21:24"
}'
```

``` bash
curl --location --request DELETE 'http://localhost:8080/delete/{id}'
```