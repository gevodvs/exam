package com.exam.Hevod.service;

import com.exam.Hevod.entity.dto.Sensor;
import com.exam.Hevod.entity.dto.SensorData;
import com.exam.Hevod.entity.dto.requst.AddSensorRequest;
import com.exam.Hevod.entity.dto.requst.UpdateSensorRequest;
import com.exam.Hevod.entity.dto.response.AddSensorResponse;
import com.exam.Hevod.entity.dto.response.GetSensorDataResponse;
import com.exam.Hevod.entity.dto.response.SensorResponse;
import com.exam.Hevod.repository.SensorRepository;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Service
@Data
@Slf4j
public class SensorService {

    private final SensorRepository sensorRepository;

    public AddSensorResponse saveSensor(AddSensorRequest addSensorRequest) {
        validateAddSensorRequest(addSensorRequest);
        Sensor saved = sensorRepository.save(buildSensor(addSensorRequest));
        return AddSensorResponse.builder()
                .id(saved.getId())
                .name(saved.getName())
                .uid(saved.getUid()).build();
    }

    @Transactional
    public List<Sensor> findAll() {
        return sensorRepository.findAll();
    }

    public void updateSensorData(UpdateSensorRequest updateSensorRequest) {
        Sensor sensor = checkUpdateSensorRequest(updateSensorRequest);

        SensorData sensorData = new SensorData();
        sensorData.setData(updateSensorRequest.getData());
        sensorData.setUpdateTime(updateSensorRequest.getUpdateTime());
        sensor.getSensorDataList().add(sensorData);

        sensorRepository.save(sensor);
    }

    public GetSensorDataResponse getSensorData(Long id) {

        Optional<Sensor> byId = sensorRepository.findById(id);
        boolean present = byId.isPresent();
        if (present) {
            Sensor sensor = byId.get();
            List<SensorData> result = Objects.nonNull(sensor.getSensorDataList()) ?
                    sensor.getSensorDataList() : Collections.EMPTY_LIST;
            List<SensorResponse> sensorResponses = result.stream()
                    .sorted(Comparator.comparing(SensorData::getData))
                    .map(sensorData -> new SensorResponse(sensorData.getUpdateTime(), sensorData.getData()))
                    .toList();
            sensorResponses = (isNull(sensor.getResultSize()) || sensor.getResultSize() <= 0) ?
                    sensorResponses : sensorResponses.stream().limit(sensor.getResultSize()).toList();
            return new GetSensorDataResponse(sensorResponses);
        }
        throw new IllegalArgumentException("Cant find sensor with such id " + id);
    }

    public void deleteSensor(Long id) {
        sensorRepository.deleteById(id);
    }

    public void deleteSensorData(Long id, LocalDateTime compareTime) {
        Optional<Sensor> byId = sensorRepository.findById(id);
        if (byId.isPresent()) {
            Sensor sensor = byId.get();
            List<SensorData> sensorDataList = sensor.getSensorDataList();
            List<SensorData> toRemove = sensorDataList.stream()
                    .filter(sensorData ->
                            sensorData.getUpdateTime().compareTo(compareTime) > 0)
                    .toList();
            toRemove.forEach(sensorDataList::remove);
            sensorRepository.save(sensor);
            log.info("delete old data for sensor with id: {}, rows removed: {}", id, toRemove.size());
        }
    }

    private void validateAddSensorRequest(AddSensorRequest addSensorRequest) {
        if (isNotBlank(addSensorRequest.getUid())) {
            if (sensorRepository.findSensorByUid(addSensorRequest.getUid()).isPresent()) {
                throw new IllegalArgumentException("There is already a sensor with the same uid exists" + addSensorRequest.getUid());
            }
        }
        if (sensorRepository.findSensorByName(addSensorRequest.getName()).isPresent()) {
            throw new IllegalArgumentException("There is already a sensor with the same name exists" + addSensorRequest.getName());
        }
    }


    private Sensor checkUpdateSensorRequest(UpdateSensorRequest updateSensorRequest) {
        Optional<Sensor> sensorByUid = sensorRepository.findSensorByUid(updateSensorRequest.getUid());

        if (sensorByUid.isPresent()) {
            if (isNull(updateSensorRequest.getData())) {
                throw new IllegalArgumentException("Update sensor data can`t be null. Uid " + updateSensorRequest.getUid());
            }
            return sensorByUid.get();
        } else {
            throw new IllegalArgumentException("Cant find sensor with such uid " + updateSensorRequest.getUid());
        }

    }


    private Sensor buildSensor(AddSensorRequest addSensorRequest) {
        Sensor sensor = new Sensor();

        boolean uidPresent = isNotBlank(addSensorRequest.getUid());
        String uid = uidPresent ? addSensorRequest.getName() : UUID.randomUUID().toString();

        sensor.setName(addSensorRequest.getName().strip());
        sensor.setUid(uid);
        sensor.setResultSize(addSensorRequest.getResultSize());
        return sensor;
    }
}
