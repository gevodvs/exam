package com.exam.Hevod.service;

import com.exam.Hevod.entity.dto.Sensor;
import lombok.Data;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Data
public class OldSensorDataCleaner {

    private final SensorService sensorService;


    @Scheduled(cron = "*/20 * * * * *")
    public void removeOldData() {
        List<Sensor> all = sensorService.findAll();
        all.forEach(sensor -> sensorService.deleteSensorData(sensor.getId(), LocalDateTime.now().minusSeconds(30L)));

    }
}
