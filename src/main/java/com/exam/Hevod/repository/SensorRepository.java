package com.exam.Hevod.repository;

import com.exam.Hevod.entity.dto.Sensor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SensorRepository extends JpaRepository<Sensor, Long> {

    Optional<Sensor> findSensorByUid(String uid);

    Optional<Sensor> findSensorByName(String name);


}
