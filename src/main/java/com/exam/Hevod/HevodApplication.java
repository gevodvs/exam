package com.exam.Hevod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class HevodApplication {

	public static void main(String[] args) {
		SpringApplication.run(HevodApplication.class, args);
	}

}
