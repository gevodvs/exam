package com.exam.Hevod.controller;

import com.exam.Hevod.entity.dto.requst.AddSensorRequest;
import com.exam.Hevod.entity.dto.requst.UpdateSensorRequest;
import com.exam.Hevod.entity.dto.response.AddSensorResponse;
import com.exam.Hevod.entity.dto.response.GetSensorDataResponse;
import com.exam.Hevod.service.SensorService;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Data
@Validated
public class SensorController {

    private final SensorService sensorService;

    @PostMapping("/add")
    public ResponseEntity<AddSensorResponse> addSensor(
            @RequestBody AddSensorRequest request) {
        return new ResponseEntity<>(sensorService.saveSensor(request), HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateSensorData(
            @RequestBody UpdateSensorRequest request) {
        sensorService.updateSensorData(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<GetSensorDataResponse> getSensorInfo(@PathVariable("id") Long id) {
        return new ResponseEntity<>(sensorService.getSensorData(id), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<AddSensorResponse> deleteSensor(@PathVariable("id") Long id) {
        sensorService.deleteSensor(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
