package com.exam.Hevod.entity.dto;


import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.Cascade;

import java.util.List;

import static org.hibernate.annotations.CascadeType.*;

@Entity
@Data
public class Sensor {
    @Id
    @GeneratedValue
    private Long id;
    private String uid;
    private String name;
    private Long resultSize;
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(
            name = "sensor_sensor_data",
            joinColumns = {@JoinColumn(name = "sensor_id")},
            inverseJoinColumns = {@JoinColumn(name = "sensor_data_id"),
            }
    )
    @Cascade(value = {ALL})
    private List<SensorData> sensorDataList;
}
