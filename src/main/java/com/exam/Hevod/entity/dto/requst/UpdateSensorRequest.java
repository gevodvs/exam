package com.exam.Hevod.entity.dto.requst;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.annotation.Nonnull;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class UpdateSensorRequest {

    @Nonnull
    private String uid;
    @Nonnull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime updateTime;
    private BigDecimal data;
}
