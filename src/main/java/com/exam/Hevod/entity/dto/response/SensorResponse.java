package com.exam.Hevod.entity.dto.response;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public record SensorResponse(LocalDateTime time, BigDecimal data) {
}
