package com.exam.Hevod.entity.dto.requst;

import jakarta.annotation.Nonnull;
import lombok.Data;

@Data
public class AddSensorRequest {
    @Nonnull
    private String name;
    private String uid;
    private Long resultSize;
}
