package com.exam.Hevod.entity.dto;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
public class SensorData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal data;
    private LocalDateTime updateTime;
    @ManyToMany(mappedBy = "sensorDataList", cascade = CascadeType.REFRESH)
    private List<Sensor> sensorList;
}
