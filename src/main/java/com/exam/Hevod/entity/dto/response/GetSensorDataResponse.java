package com.exam.Hevod.entity.dto.response;

import java.util.List;


public record GetSensorDataResponse(List<SensorResponse> result) {

}
