package com.exam.Hevod.entity.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddSensorResponse {

    private String name;
    private String uid;
    private Long id;
}
